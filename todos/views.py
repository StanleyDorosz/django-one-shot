from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = TodoListForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todolist": todolist,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
  todolist = TodoList.objects.get(id=id)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = TodoItemForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save()
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "todoitem": todoitem,
        "form": form,
    }
    return render(request, "todos/itemedit.html", context)
